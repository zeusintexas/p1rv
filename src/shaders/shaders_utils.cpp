/*
 * shaders_util.cpp: Utilities for OpenGL shaders generation and compilation
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#include "shaders/shaders_utils.h"

#include <iostream>
#include <assert.h>

#if _WIN32

#include <Windows.h>

#endif

#include "shaders/shaders_loader.h"

using namespace std;

int compile(GLuint *shaderptr, GLenum type, const char *fname) {
    int ret = 0;
    ShaderLoader shaderload;
    char **listptr;
    int listsize;
    const int MAX_INFO_LOG_SIZE = 400;
    char infolog[MAX_INFO_LOG_SIZE];

    if (type != GL_VERTEX_SHADER && type != GL_FRAGMENT_SHADER) {
        // attempting to create other than vertex or fragment shader
        cerr << "[ERROR] invalid shader type supplied for " << fname << ". Supported types includes: vertex,fragment"
             << endl;
        *shaderptr = 0;
        return 3;
    }

    *shaderptr = glCreateShader(type);
    if (*shaderptr == 0) {
        cerr << "[ERROR] OpenGL failed to generate a shader" << endl;
        return 2;
    }

    shaderload.loadFile(fname);
    if (!shaderload.getSizeAndList(&listsize, &listptr)) {
        cout << "[ERROR] Could not load shader: " << fname << endl;
        glDeleteShader(*shaderptr);
        *shaderptr = 0;
        return 4;
    }

    // Compile the shader
    glShaderSource(*shaderptr, listsize, (const char **) listptr, nullptr);
    glCompileShader(*shaderptr);

    glGetShaderiv(*shaderptr, GL_COMPILE_STATUS, &ret);
    if (!ret) {
        cerr << "[ERROR] could not compile " << fname << endl;
        glGetShaderiv(*shaderptr, GL_INFO_LOG_LENGTH, &ret);
        glGetShaderInfoLog(*shaderptr, MAX_INFO_LOG_SIZE, nullptr, &(infolog[0]));
        cerr << "----------------------------------------" << endl;
        cerr << infolog << endl;
        cerr << "----------------------------------------" << endl;
        if (ret > MAX_INFO_LOG_SIZE)
            cerr << "[WARN] log information may be incomplete (consider increasing MAX_INFO_LOG_SIZE)" << endl;
        glDeleteShader(*shaderptr);
        *shaderptr = 0;
        return 5;
    }

    return 0;
}

int generate_shaders(GLuint *programptr, GLuint *vsptr, const char *vsfname, GLuint *fsptr, const char *fsfname) {
    assert(programptr && vsptr);

    int ret = 0;
    const int MAX_INFO_LOG_SIZE = 400;
    char infolog[MAX_INFO_LOG_SIZE];

    // Ensure shaders are supported by GLEW
    if (!GLEW_ARB_shading_language_100 || !GLEW_ARB_shader_objects) {
        cerr << "[ERROR] shaders are NOT supported by this implementation of GLEW!" << endl;
        *programptr = *vsptr = *fsptr = 0;
        return 1;
    }

    *programptr = glCreateProgram();
    if (*programptr == 0) {
        cerr << "[ERROR] OpenGL failed to generate a shader program" << endl;
        *vsptr = *fsptr = 0;
        return 2;
    }

    // Compile the shaders
    if (vsfname) {
        if ((ret = compile(vsptr, GL_VERTEX_SHADER, vsfname)) != 0) {
            glDeleteProgram(*programptr);
            *programptr = *vsptr = *fsptr = 0;
            return ret;
        }
        glAttachShader(*programptr, *vsptr);
    } else {
        *vsptr = 0;
    }

    if (fsfname) {
        if ((ret = compile(fsptr, GL_FRAGMENT_SHADER, fsfname)) != 0) {
            if (*vsptr != 0)
                glDeleteShader(*vsptr);
            glDeleteProgram(*programptr);
            *programptr = *vsptr = *fsptr = 0;
            return ret;
        }
        glAttachShader(*programptr, *fsptr);
    } else {
        *vsptr = 0;
    }

    // Link both shaders into program
    glLinkProgram(*programptr);
    glGetProgramiv(*programptr, GL_LINK_STATUS, &ret);
    if (!ret) {
        cerr << "[ERROR] could not link " << vsfname << " and " << fsfname << endl;
        glGetProgramiv(*programptr, GL_INFO_LOG_LENGTH, &ret);
        glGetProgramInfoLog(*programptr, MAX_INFO_LOG_SIZE, nullptr, infolog);
        cerr << "----------------------------------------" << endl;
        cerr << infolog << endl;
        cerr << "----------------------------------------" << endl;
        if (ret > MAX_INFO_LOG_SIZE)
            cerr << "[WARN] log information may be incomplete (consider increasing MAX_INFO_LOG_SIZE)" << endl;
        glDeleteProgram(*programptr);
        if (*vsptr != 0)
            glDeleteShader(*vsptr);
        if (*fsptr != 0)
            glDeleteShader(*fsptr);
        *programptr = *vsptr = *fsptr = 0;
        return 6;
    }

    return 0;
}