/*
 * utils.cpp: Utilities for OpenStereo
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 16/11/18.
 *
*/

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glut.h>

void renderWarning() {
    glPushAttrib(GL_VIEWPORT_BIT | GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT
                 | GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TRANSFORM_BIT);

    glDisable(GL_DEPTH_TEST);

    glClearColor(0.2, 0.2, 0.2, 0.2);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.5, 1.5);
    //gluPerspective(60.0f, 0.5, 1.0, 40.0);
    //glTranslatef(0.0f, 0.0f, -5.0f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    //white backing triangle
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    glVertex3f(1.0 / 2.0, 7.0 / 8.0 + 1.0 / 16.0, -1.0);
    glVertex3f(1.0 / 8.0 - 1.0 / 16.0, 1.0 / 8.0 - 1.0 / 16.0, -1.0);
    glVertex3f(7.0 / 8.0 + 1.0 / 16.0, 1.0 / 8.0 - 1.0 / 16.0, -1.0);
    glEnd();

    //black backing triangle
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_TRIANGLES);
    glVertex3f(1.0 / 2.0, 7.0 / 8.0 + 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 8.0 - 1.0 / 32.0, 1.0 / 8.0 - 1.0 / 32.0, -1.0);
    glVertex3f(7.0 / 8.0 + 1.0 / 32.0, 1.0 / 8.0 - 1.0 / 32.0, -1.0);
    glEnd();

    //yellow main triangle
    glColor3f(1.0f, 1.0f, 0.0f);
    glBegin(GL_TRIANGLES);
    glVertex3f(1.0 / 2.0, 7.0 / 8.0, -1.0);
    glVertex3f(1.0 / 8.0, 1.0 / 8.0, -1.0);
    glVertex3f(7.0 / 8.0, 1.0 / 8.0, -1.0);
    glEnd();

    //white !
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    glVertex3f(1.0 / 2.0 + 1.0 / 32.0, 5.0 / 8.0 + 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 32.0, 5.0 / 8.0 + 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 32.0, 3.0 / 8.0 - 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 + 1.0 / 32.0, 3.0 / 8.0 - 1.0 / 32.0, -1.0);

    glVertex3f(1.0 / 2.0 + 1.0 / 32.0, 2.0 / 8.0 + 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 32.0, 2.0 / 8.0 + 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 32.0, 2.0 / 8.0 - 1.0 / 32.0, -1.0);
    glVertex3f(1.0 / 2.0 + 1.0 / 32.0, 2.0 / 8.0 - 1.0 / 32.0, -1.0);
    glEnd();

    //black !
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_QUADS);
    glVertex3f(1.0 / 2.0 + 1.0 / 64.0, 5.0 / 8.0 + 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 64.0, 5.0 / 8.0 + 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 64.0, 3.0 / 8.0 - 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 + 1.0 / 64.0, 3.0 / 8.0 - 1.0 / 64.0, -1.0);

    glVertex3f(1.0 / 2.0 + 1.0 / 64.0, 2.0 / 8.0 + 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 64.0, 2.0 / 8.0 + 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 - 1.0 / 64.0, 2.0 / 8.0 - 1.0 / 64.0, -1.0);
    glVertex3f(1.0 / 2.0 + 1.0 / 64.0, 2.0 / 8.0 - 1.0 / 64.0, -1.0);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();  //this may not work
}