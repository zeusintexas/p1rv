/*
 * log.h: Dead simple .h logging to stdout/stderr
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 16/11/18.
 *
*/

#ifndef OPENSTEREO_LOG_H
#define OPENSTEREO_LOG_H

#include <iostream>

#define log_fatal std::cerr << "[FATAL] "
#define log_error std::cerr << "[ERROR] "
#define log_warning std::cout << "[WARN ] "
#define log_info std::cout << "[INFO ] "
#define log_debug std::cout << "[DEBUG] "

#endif //OPENSTEREO_LOG_H