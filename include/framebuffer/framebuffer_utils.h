/*
 * framebuffer_utils.h: Utilities definitions for OpenGL framebuffers
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 * Copyright (c) 2011 Daniel Schroeder
 *
 * This file is adapted from MaGLi.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#ifndef OPENSTEREO_FRAMEBUFFER_FRAMBEBUFFER_UTILS_H
#define OPENSTEREO_FRAMEBUFFER_FRAMBEBUFFER_UTILS_H

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glew.h>
#include <GL/glut.h>

/**
	Creates a simple framebuffer
	@return non-zero value on error
**/
int create_framebuffer(GLuint *fbobjptr);

/**
	Creates a simple framebuffer, using all textures
	@return non-zero value on error
**/
int create_textured_framebuffer(GLuint *fbobjptr, GLuint *deptexptr, GLuint *texobjptr, int width, int height);

/**
	Generate a 2D texture in texobjptr from bufptr
	@return non-zero value on error
**/
int generate_texture_2d(GLuint *texobjptr, int width, int height, float *bufptr);

/**
	Creates a RGBA texture in texobjptr using random data
	@return non-zero value on error
**/
int generate_texture_rgba(GLuint *texobjptr, int width, int height);

/**
	Creates a RGBA texture in texobjptr using data from data
	@return non-zero value on error
**/
int generate_texture_rgba(GLuint *texobjptr, int width, int height, float *data);

/**
	Creates a simple render buffer
	@return non-zero value on error
**/
int create_render_buffer(GLuint *rbobjptr, GLenum internalformat, int width, int height);

#endif //OPENSTEREO_FRAMEBUFFER_FRAMBEBUFFER_UTILS_H