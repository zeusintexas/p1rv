/*
 * openstereo.h: OpenStereo Autostereogram OpenGL renderer general includes
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 10/10/18.
 *
*/

#ifndef OPENSTEREO_OPENSTEREO_H
#define OPENSTEREO_OPENSTEREO_H

#ifdef __cplusplus
extern "C" {
#endif

// Types definitions
typedef void (*GLUT_displayFunc)();

typedef void (*GLUT_reshapeFunc)(int width, int height);

typedef void (*GLUT_keyboardFunc)(unsigned char key, int x, int y);

typedef void (*GLUT_motionFunc)(int x, int y);

typedef void (*GLUT_mouseFunc)(int button, int state, int x, int y);

// Stereogram functions
/**
 * Initialize the OpenStereo library. Called once at the very beginning of the program.
 * @param argc The address of the program's argument count
 * @param argv The program's argument
 * @return true if the library was successfully initialized
 */
bool stereogramInit(int *argc, char **argv);

/**
 * Destroys the OpenStereo library. Called at the very ending of the program,
 * so memory checkers like Valgrind won't complain.
 */
void stereogramDestroy();

/**
 * The OpenStereo implementation required as argument to glutDisplayFunc.
 * Note: any custom user function requiring this callback passed as parameter to stereogramDisplay()
 *  will be called afterwards.
 * @param fun An optional GLUT_motionFunc user function to call afterwards
 * @return A GLUT_motionFunc function handle
 */
GLUT_displayFunc stereogramDisplay(GLUT_displayFunc fun = nullptr);

/**
 * The OpenStereo implementation required as argument to glutReshapeFunc.
 * Note: any custom user function requiring this callback passed as parameter to stereogramReshape()
 *  will be called afterwards.
 * @param fun An optional GLUT_motionFunc user function to call afterwards
 * @return A GLUT_motionFunc function handle
 */
GLUT_reshapeFunc stereogramReshape(GLUT_reshapeFunc fun = nullptr);

/**
 * The OpenStereo implementation required as argument to glutKeyboardFunc.
 * Note: any custom user function requiring this callback passed as parameter to stereogramKeyboard()
 *  will be called afterwards.
 * @param fun An optional GLUT_motionFunc user function to call afterwards
 * @return A GLUT_motionFunc function handle
 */
GLUT_keyboardFunc stereogramKeyboard(GLUT_keyboardFunc fun = nullptr);

/**
 * @deprecated
 * The OpenStereo implementation required as argument to glutMotionFunc.
 * Note: any custom user function requiring this callback passed as parameter to stereogramMotion()
 *  will be called afterwards.
 * @param fun An optional GLUT_motionFunc user function to call afterwards
 * @return A GLUT_motionFunc function handle
 */
GLUT_motionFunc stereogramMotion(GLUT_motionFunc fun = nullptr);

/**
 * @deprecated
 * The OpenStereo implementation required as argument to glutMouseFunc.
 * Note: any custom user function requiring this callback passed as parameter to stereogramMouse()
 *  will be called afterwards.
 * @param fun An optional GLUT_mouseFunc user function to call afterwards
 * @return A GLUT_mouseFunc function handle
 */
GLUT_mouseFunc stereogramMouse(GLUT_mouseFunc fun = nullptr);

// Windows functions
/**
 * Get OpenStereo-managed window's width
 * @return int Window width
 */
int getWinXRes();

/**
 * Get OpenStereo-managed window's height
 * @return int Window height
 */
int getWinYRes();

#ifdef __cplusplus
};
#endif

#endif //OPENSTEREO_OPENSTEREO_H