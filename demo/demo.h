/*
 * deomo.h: Definitions of a test program for the OpenGL autostereogram renderer
 *
 * Copyright (c) 2018 Matis Granger, Gustave Bainier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * File created on 26/11/18.
 *
*/

#pragma once

#if _WIN32

#include <Windows.h>

#endif

#include <GL/glew.h>
#include <GL/glut.h>

GLvoid display();

GLvoid reshape(int w, int h);

GLvoid keyboard(unsigned char touche, int x, int y);

GLvoid mouse(int bouton, int etat, int x, int y);


GLvoid motion(int x, int y);